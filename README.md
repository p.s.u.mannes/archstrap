# Archstrap
&nbsp; 
<img src="https://gitlab.com/p.s.u.mannes/archstrap/-/raw/master/public/ansible_arch.png" alt="Arch logo">&nbsp; 

### License
[GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html)
&nbsp; 

### Disclaimer
> <span style="color:red">**Warning:** Running this install results in an
XFCE desktop with Arch. However, this is a personalized install 
that comes with added features including a customized neovim configuration.
Consider changing the username in archstrap.yml and forking the code to adjust
it to your liking.
</span>
&nbsp; 

<img src="https://gitlab.com/p.s.u.mannes/archstrap/-/raw/master/public/desktop.png" alt="Desktop after install">&nbsp; &nbsp; 

### Boot into live Arch ISO
<https://wiki.archlinux.org/title/USB_flash_installation_medium>
&nbsp; 

### Test internet connection
```
ping 1.1.1.1
```
&nbsp; 

### If no internet, connect to wifi
```
iwctl
device list
station [interface] scan
station [interface] get-networks
station [interface] connect [network name]
station [interface] show
```
&nbsp; 

### Configure disk
```
# sda is used as an example
lsblk

# wipe disk
sfdisk --delete /dev/sda

# partition EFI, filesystem, and swap
fdisk /dev/sda
g
n -> enter -> enter -> +1G
t -> 1
n -> enter -> enter -> +460G
n -> enter -> enter -> enter
t -> enter -> 19
p
w

# make file system
mkfs.fat -F 32 /dev/sda1
mkfs.ext4 /dev/sda2

# mount file systems
mount /dev/sda2 /mnt
mount --mkdir /dev/sda1 /mnt/boot

# make swap
mkswap /dev/sda3
swapon /dev/sda3
```
&nbsp; 

### Configure base system
```
# install base packages, -K generates pacman keyring
pacstrap -K /mnt base linux linux-firmware ansible git

# enable persistent mount
genfstab -U /mnt >> /mnt/etc/fstab

# chroot into new system
arch-chroot /mnt

# set root password
passwd
```
&nbsp; 

### Bootstrap with ansible
```
# clone archstrap repo
git clone https://gitlab.com/p.s.u.mannes/archstrap.git

# run archstrap playbook
ansible-playbook archstrap/archstrap.yml

# set user password
passwd <user>

# exit chroot and reboot
exit
reboot
```
&nbsp; 

### Wifi after reboot
```
# connect to wifi
nmcli device wifi list
nmcli device wifi connect <SSID> password <password>
nmcli device status
```
&nbsp; 

### Touchpad tapping 
- XFCE menu: Mouse and Touchpad
- Device: Touchpad


