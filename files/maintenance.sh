#!/bin/bash

# Update mirror list
sudo reflector --verbose --latest 10 --sort rate --save /etc/pacman.d/mirrorlist

# Update and refresh keys if failed
sudo yay --noconfirm || sudo pacman-keys --refresh-keys && sudo yay --noconfirm

# Prune cache
yes | sudo paccache -rk1

# Remove orphans
sudo pacman -Rns $(pacman -Qtdq)
