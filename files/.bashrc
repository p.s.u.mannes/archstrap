# start x on first session
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
    startx
fi

# stop here if not in interactive mode
[[ $- != *i* ]] && return

# don't put duplicate lines or lines starting with space in the history.
HISTCONTROL=ignoreboth

HISTSIZE=10000
HISTFILESIZE=10000

# append to the history file in real-time, don't overwrite it
shopt -s histappend
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# alias commands
alias ls="ls --color"
alias dir="dir --color=auto"
alias grep="grep --color=auto"

alias g="git status"
alias ac="git add . && git commit -m '...'"
alias gps="git push"
alias gpl="git pull"
alias gl="git log -p"

alias vi="vim"
alias yay="yay --noconfirm"

alias upd="bash ~/maintenance.sh"
